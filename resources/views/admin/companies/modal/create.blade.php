

            <form class="forms-sample" action="{{route('companies.store')}}" method="post" enctype="multipart/form-data" >
                @csrf

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="company">Company Name</label>
                            <input type="text" name="name" class="form-control" id="company" placeholder="Company Name">

                        </div>
                        <div class="form-group">
                            <label for="owner">Owner</label>
                            <input type="text" name="owner" class="form-control" id="owner" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="Company Name">
                        </div>

                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="Company Name">
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>File upload</label>
                            <input type="file" name="img[]" class="file-upload-default">
                        <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                            <div class="input-group-append">
                                <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                            </div>
                        </div>
                        </div>

                        <div class="form-group">
                            <div class="form-check form-check-flat">
                                <label class="form-check-label">
                                <input type="checkbox" name="is_active" class="form-check-input">
                                Is Active?
                                <i class="input-helper"></i></label>
                            </div>
                        </div>

                    </div>

                </div>

                <button type="submit" class="btn btn-success mr-2">Save</button>
                <a class="btn btn-light" href="{{route('companies.index')}}">Cancel</a>
            </form>




