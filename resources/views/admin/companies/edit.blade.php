@extends('layouts.admin')
@section('title','Company List')
@push('css_style')
<link rel="stylesheet" href="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}" />
@endpush

@section('content')

<div class="card">
    <div class="card-body">
      <h4 class="card-title">Edit Company</h4>

      <form class="forms-sample form-horizontal form-groups-bordered" action="{{route('companies.update',$company->id)}}" method="post" enctype="multipart/form-data" >
            {{-- @csrf --}}
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="company">Company Name</label>
                        <input type="text" name="name" class="form-control" id="company" placeholder="Company Name" value="{{$company->name}}">

                    </div>
                    <div class="form-group">
                        <label for="owner">Owner</label>
                        <input type="text" name="owner" class="form-control" id="owner" placeholder="Company Owner" value="{{$company->owner}}">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Company Phone" value="{{$company->phone}}">
                    </div>

                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{$company->email}}">
                    </div>
                    {{-- <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password" >
                    </div> --}}

                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" class="form-control" id="address" placeholder="Company Address" value="{{$company->address}}">
                    </div>

                </div>

                <div class="col-md-6">
                    <div class="form-group">
						<label class="col-sm-3 control-label">Company Logo</label>
						<div class="col-sm-9">
							<input type="file" class="form-control dropify" name="image" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" data-default-file="{{asset('uploads/'.$company->logo)}}">
						</div>

					</div>

                    <div class="form-group">
                        <div class="form-check form-check-flat">
                            <label class="form-check-label">
                            <input type="checkbox" name="is_active" class="form-check-input" {{$company->is_active==1?'checked':''}}>
                            Is Active?
                            <i class="input-helper"></i></label>
                        </div>
                    </div>

                </div>

            </div>

            <button type="submit" class="btn btn-success mr-2">Upate</button>
            <a class="btn btn-light" href="{{route('companies.index')}}">Cancel</a>
      </form>
    </div>
  </div>

@endsection


@push('script_js')
  <!-- Plugin js for this page-->
  <script src="{{asset('backend/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('backend/js/data-table.js')}}"></script>
@endpush
