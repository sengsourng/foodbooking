@extends('layouts.admin')
@section('title','Company List')
@push('css_style')
    <link rel="stylesheet" href="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}" />
 <!-- SweetAlert2 -->
 <link rel="stylesheet" href="{{asset('backend/sweetalert2/sweetalert2.min.css') }}">
    @endpush


@section('content')


<div class="card">
    <div class="card-body">

      <h4 class="card-title">Company List  <a class="btn btn-primary btn-sm pull-right ajax-modal" href="{{route('companies.create')}}">Create</a></h4>
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                    <th>ID #</th>
                    <th>Comapny Name</th>
                    <th>Owner</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($companies as $row)
                <tr>
                    <td>{{$row->id}}</td>
                    <td>
                        <div class="wrapper d-flex align-items-center py-2 border-bottom">
                            <img class="img-sm rounded-circle" src="{{asset('uploads/'. $row->logo)}}" alt="profile">
                            <div class="wrapper ml-3">
                                <h6 class="ml-1 mb-1"> {{$row->name}}</h6>
                                <small class="text-muted mb-0"><i class="mdi mdi-map-marker-outline mr-1"></i>{{$row->address}}</small>
                            </div>
                            <div class="badge badge-pill badge-info ml-auto px-1 py-1"><i class="mdi mdi-check font-weight-bold"></i></div>
                        </div>



                    </td>
                    <td>{{$row->owner}}</td>
                    <td>{{$row->phone}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->address}}</td>
                    <td>
                      <label class="badge badge-info">On hold</label>
                    </td>


                    <td class="text-center">
                        <a href="{{ route('companies.edit',$row->id) }}" class="badge badge-danger">
                            <i class="mdi mdi-pencil"></i> Edit
                        </a>
                        <a href="javascript:deleteItem({{ $row->id }})" class="badge badge-info" id="btnDelete">
                            <i class="mdi mdi-delete"></i> Delete
                        </a>
                        <form id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('companies.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                    </td>
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>


@endsection


@push('script_js')

 <!-- SweetAlert2 -->
 <script src="{{asset('backend/sweetalert2/sweetalert2.min.js') }}"></script>

  <!-- Plugin js for this page-->
  <script src="{{asset('backend/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('backend/js/data-table.js')}}"></script>

  <script  src="{{asset('backend/js/script.js')}}"></script>


  <script type="text/javascript">
    function deleteItem(id){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('frmDeleteItem-'+id).submit();
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        }
      })
    }
</script>

@endpush
