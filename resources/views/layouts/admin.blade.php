<!DOCTYPE html>
<html lang="en">


<head>
    @include('layouts.inc.header')
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('layouts.partials.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <!-- partial:partials/_settings-panel.html -->
            @include('layouts.partials.settings_panel')

            @include('layouts.partials.right_sidebar')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
            @include('layouts.partials.sidebar')
        <!-- partial -->
        <div class="content-wrapper">

            {{-- @include('admin.dashboard') --}}
            @yield('content')

          <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2021 <a href="#">សេង ស៊ង់</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">វិទ្យាសាស្រ្តកុំព្យូទ័រ​ជំនាន់​២ <i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row-offcanvas ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{asset('backend/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="{{asset('backend/vendors/jquery-bar-rating/jquery.barrating.min.js')}}"></script>
  <script src="{{asset('backend/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('backend/vendors/raphael/raphael.min.js')}}"></script>
  <script src="{{asset('backend/vendors/morris.js/morris.min.js')}}"></script>
  <script src="{{asset('backend/vendors/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- End plugin js for this page-->

  <!--  Dropify  -->
<script type="text/javascript" src="{{ asset('backend') }}/js/dropify.js"></script>
  <!-- inject:js -->
  <script src="{{asset('backend/js/off-canvas.js')}}"></script>
  <script src="{{asset('backend/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('backend/js/misc.js')}}"></script>
  <script src="{{asset('backend/js/settings.js')}}"></script>
  <script src="{{asset('backend/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('backend/js/dashboard.js')}}"></script>
  <!-- End custom js for this page-->
  <script>
      $('.dropify').dropify();

  </script>
  @stack('script_js')
</body>


</html>
