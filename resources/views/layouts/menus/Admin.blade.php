<li class="nav-item">
    <a class="nav-link" href="{{url('/admin/home')}}">
    <i class="icon-rocket menu-icon"></i>
    <span class="menu-title">Dashboard</span>
    <span class="badge badge-success">New</span>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#tables1" aria-expanded="false" aria-controls="tables">
    <i class="fa fa-address-book-o menu-icon"></i>
    <span class="menu-title">All People</span>
    <span class="badge badge-info">4</span>
    </a>
    <div class="collapse" id="tables1">
    <ul class="nav flex-column sub-menu">
        <li class="nav-item"> <a class="nav-link" href="{{route('companies.index')}}">Companies</a></li>
        <li class="nav-item"> <a class="nav-link" href="#">Delivers</a></li>
        <li class="nav-item"> <a class="nav-link" href="#">Venders</a></li>
        <li class="nav-item"> <a class="nav-link" href="#">Categories</a></li>
    </ul>
    </div>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{url('/admin/home')}}">
        <i class="fa fa-id-badge menu-icon"></i>
        <span class="menu-title">Customers</span>
        <span class="badge badge-info">4</span>
    </a>
</li>



{{-- Users --}}

<li class="nav-item">
    <a class="nav-link" href="{{url('#')}}">
        <i class="fa fa-group menu-icon"></i>
        <span class="menu-title">Manage Users</span>
        <span class="badge badge-info">4</span>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{url('#')}}">
        <i class="fa fa-cogs menu-icon"></i>
        <span class="menu-title">Settings</span>
        <span class="badge badge-info">4</span>
    </a>
</li>
