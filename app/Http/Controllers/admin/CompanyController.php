<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use phpDocumentor\Reflection\Types\Nullable;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=[];
        $data['companies']=Company::get();

        // dd($data['companies']);

        return view('admin.companies.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    dd($request->all());
    // $this->validate($request, [
    //     'name' => 'required|string|max:250',
    //     'phone' => 'required|string|max:191',
    //     'address' => 'required',
    //     'email' => 'required|string|email|max:191|unique:users',
    //     'password' => 'required|string|min:6|confirmed',
    //     'image' => 'nullable|image|max:5120',
    // ]);

    $ImageName='profile.png';
    if ($request->hasFile('image')){
       $image = $request->file('image');
       $ImageName = time().'.'.$image->getClientOriginalExtension();
       Image::make($image)->resize(400, 400)->save(base_path('public/uploads/companies/').$ImageName);
    }

    $user = new User();
    $user->name = $request->name;
    $user->email = $request->email;
    $user->password = Hash::make($request->password);
    $user->user_type = 'Company';
    $user->phone = $request->phone;
    $user->image = 'companies/'.$ImageName;
    $user->save();
// dd($user);
    $company = new Company();
    $company->user_id = $user->id;
    $company->name=$request->name;
    $company->owner=$request->owner;
    $company->email=$request->email;
    $company->phone=$request->phone;
    $company->address=$request->address;
    $company->logo = 'companies/'.$ImageName;
    $company->is_active=$request->is_active==true?1:0;

    $company->save();

    return redirect('admin/companies')->with('success', 'Add Successfull !');
    // return back();



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.companies.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        // dd($company);
        $data['company']=Company::findOrFail($id);
        // dd($company);
        return view('admin.companies.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);
       // $company->user_id = $user->id;

       $ImageName=null;
       if ($request->hasFile('image')){
          $image = $request->file('image');
          $ImageName = time().'.'.$image->getClientOriginalExtension();
          Image::make($image)->resize(400, 400)->save(base_path('public/uploads/companies/').$ImageName);
          unlink(base_path('public/uploads/'.$company->logo));
          $company->logo = 'companies/'.$ImageName;
        }else{
            $company->logo = $company->logo;
       }


        $company->name=$request->name;
        $company->owner=$request->owner;
        $company->email=$request->email;
        $company->phone=$request->phone;
        $company->address=$request->address;
        $company->is_active=$request->is_active==true?1:0;

        $company->save();


        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
