<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('company_id')->nullable();
            $table->foreignId('customer_id')->nullable();  //this is the foreign key that links to the customer table. It refers to the customer who ordered the food.

            $table->date('order_date')->nullable();
            $table->float('total_amount')->nullable();
            $table->tinyInteger('order_status')->nullable(); //status of order (0-pending, 1-confirmed, 2-cancelled).
            $table->foreignId('processed_by')->nullable();  // this is a foreign key that connects or links to the user table. It refers to the user who processed the transaction.

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
