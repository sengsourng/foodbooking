<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('company_id')->nullable();
            $table->foreignId('menu_id')->nullable(); //foreign key that links to the menu table.
            $table->integer('score')->nullable(); //score provided by the customer (1-5 scale, 1 as the lowest and 5 as the highes
            $table->text('remarks')->nullable(); //comments, suggestions and recommendations of the customers.
            $table->date('date_recorded')->nullable();
            $table->foreignId('customer_id')->nullable(); //foreign key that links to customer information.

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
