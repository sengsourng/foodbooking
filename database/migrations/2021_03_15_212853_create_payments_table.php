<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('company_id')->nullable();
            $table->foreignId('order_id')->nullable();
            $table->float('amount')->nullable(); //amount paid by the customer.
            $table->foreignId('paid_by')->nullable(); // id of person who paid the transactions.
            $table->date('payment_date')->nullable(); //date of payment.
            $table->foreignId('processed_by')->nullable(); //this is a foreign key that connects or links to the user table. It refers to the user who processed the transaction.

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
